-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE TABLE "action" -----------------------------------
-- CREATE TABLE "action" ---------------------------------------
CREATE TABLE `action` ( 
	`title` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`type` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	PRIMARY KEY ( `title` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "event" ------------------------------------
-- CREATE TABLE "event" ----------------------------------------
CREATE TABLE `event` ( 
	`uid` Int( 255 ) AUTO_INCREMENT NOT NULL,
	`status` Int( 255 ) NOT NULL DEFAULT '4',
	`owner` Int( 255 ) NOT NULL DEFAULT '1',
	`group` Int( 255 ) NOT NULL DEFAULT '1',
	`unixperms` Int( 255 ) NOT NULL,
	`description` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	PRIMARY KEY ( `uid` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "implemented_action" -----------------------
-- CREATE TABLE "implemented_action" ---------------------------
CREATE TABLE `implemented_action` ( 
	`action` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`related_uid` Int( 255 ) NOT NULL,
	`status` Int( 255 ) NOT NULL,
	PRIMARY KEY ( `action`, `related_uid` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "privilege" --------------------------------
-- CREATE TABLE "privilege" ------------------------------------
CREATE TABLE `privilege` ( 
	`who` Int( 255 ) NOT NULL DEFAULT '0',
	`group` Int( 255 ) NOT NULL DEFAULT '2',
	`action` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`related_uid` Int( 255 ) NOT NULL,
	`status` Int( 255 ) NOT NULL DEFAULT '4',
	`type` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	PRIMARY KEY ( `who`, `group`, `action`, `related_uid`, `status`, `type` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "user" -------------------------------------
-- CREATE TABLE "user" -----------------------------------------
CREATE TABLE `user` ( 
	`uid` Int( 255 ) AUTO_INCREMENT NOT NULL,
	`group_memberships` Int( 255 ) NOT NULL,
	`unixperms` Int( 255 ) NOT NULL DEFAULT '500',
	`status` Int( 255 ) NOT NULL DEFAULT '4',
	`username` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`password` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	PRIMARY KEY ( `uid` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 17;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- Dump data of "action" -----------------------------------
INSERT INTO `action`(`title`,`type`) VALUES ( 'delete', 'object' );
INSERT INTO `action`(`title`,`type`) VALUES ( 'execute', 'object' );
INSERT INTO `action`(`title`,`type`) VALUES ( 'join', 'table' );
INSERT INTO `action`(`title`,`type`) VALUES ( 'read', 'object' );
INSERT INTO `action`(`title`,`type`) VALUES ( 'view', 'table' );
INSERT INTO `action`(`title`,`type`) VALUES ( 'write', 'object' );
-- ---------------------------------------------------------


-- Dump data of "event" ------------------------------------
INSERT INTO `event`(`uid`,`status`,`owner`,`group`,`unixperms`,`description`) VALUES ( '1', '2', '1', '1', '500', 'MySQL Camp' );
INSERT INTO `event`(`uid`,`status`,`owner`,`group`,`unixperms`,`description`) VALUES ( '2', '4', '1', '4', '500', 'Microsoft Keynote' );
-- ---------------------------------------------------------


-- Dump data of "implemented_action" -----------------------
INSERT INTO `implemented_action`(`action`,`related_uid`,`status`) VALUES ( 'delete', '1', '4' );
INSERT INTO `implemented_action`(`action`,`related_uid`,`status`) VALUES ( 'execute', '1', '4' );
INSERT INTO `implemented_action`(`action`,`related_uid`,`status`) VALUES ( 'join', '1', '1' );
INSERT INTO `implemented_action`(`action`,`related_uid`,`status`) VALUES ( 'join', '2', '2' );
INSERT INTO `implemented_action`(`action`,`related_uid`,`status`) VALUES ( 'read', '1', '4' );
INSERT INTO `implemented_action`(`action`,`related_uid`,`status`) VALUES ( 'view', '2', '4' );
INSERT INTO `implemented_action`(`action`,`related_uid`,`status`) VALUES ( 'write', '2', '0' );
-- ---------------------------------------------------------


-- Dump data of "privilege" --------------------------------
INSERT INTO `privilege`(`who`,`group`,`action`,`related_uid`,`status`,`type`) VALUES ( '1', '3', 'delete', '1', '4', 'object' );
INSERT INTO `privilege`(`who`,`group`,`action`,`related_uid`,`status`,`type`) VALUES ( '1', '3', 'execute', '1', '4', 'object' );
INSERT INTO `privilege`(`who`,`group`,`action`,`related_uid`,`status`,`type`) VALUES ( '1', '3', 'join', '1', '4', 'object' );
INSERT INTO `privilege`(`who`,`group`,`action`,`related_uid`,`status`,`type`) VALUES ( '1', '3', 'join', '2', '4', 'object' );
INSERT INTO `privilege`(`who`,`group`,`action`,`related_uid`,`status`,`type`) VALUES ( '1', '3', 'read', '1', '4', 'object' );
INSERT INTO `privilege`(`who`,`group`,`action`,`related_uid`,`status`,`type`) VALUES ( '1', '3', 'view', '2', '4', 'object' );
INSERT INTO `privilege`(`who`,`group`,`action`,`related_uid`,`status`,`type`) VALUES ( '1', '3', 'write', '2', '4', 'object' );
INSERT INTO `privilege`(`who`,`group`,`action`,`related_uid`,`status`,`type`) VALUES ( '2', '1', 'execute', '1', '4', 'object' );
INSERT INTO `privilege`(`who`,`group`,`action`,`related_uid`,`status`,`type`) VALUES ( '2', '2', 'join', '1', '4', 'table' );
INSERT INTO `privilege`(`who`,`group`,`action`,`related_uid`,`status`,`type`) VALUES ( '3', '1', 'write', '2', '4', 'object' );
INSERT INTO `privilege`(`who`,`group`,`action`,`related_uid`,`status`,`type`) VALUES ( '11', '3', 'delete', '1', '4', 'object' );
INSERT INTO `privilege`(`who`,`group`,`action`,`related_uid`,`status`,`type`) VALUES ( '11', '3', 'join', '1', '4', 'object' );
INSERT INTO `privilege`(`who`,`group`,`action`,`related_uid`,`status`,`type`) VALUES ( '11', '3', 'read', '1', '4', 'object' );
-- ---------------------------------------------------------


-- Dump data of "user" -------------------------------------
INSERT INTO `user`(`uid`,`group_memberships`,`unixperms`,`status`,`username`,`password`) VALUES ( '1', '3', '500', '4', 'root', '1' );
INSERT INTO `user`(`uid`,`group_memberships`,`unixperms`,`status`,`username`,`password`) VALUES ( '2', '2', '500', '4', 'xaprb', '1' );
INSERT INTO `user`(`uid`,`group_memberships`,`unixperms`,`status`,`username`,`password`) VALUES ( '3', '3', '500', '4', 'sakila', '1' );
INSERT INTO `user`(`uid`,`group_memberships`,`unixperms`,`status`,`username`,`password`) VALUES ( '4', '3', '500', '4', 'tam', '1' );
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


