import Router from 'koa-router';
import {Sequelize} from '../models/db';
import User from '../models/user';
import jwt from 'jsonwebtoken';
import CONFIG from '../config';
import btoa from 'btoa';

const router = new Router();

router.post('/login', async (ctx, next)=> {
    let {username} = ctx.request.body;
    let {password} = ctx.request.body;

    password = btoa(password);
    let user = await User.find({
        where:{
            username: username,
            password: password
        }
    });
    if(user){
        if((user.group_memberships & 1) > 0){
            var token = jwt.sign({
                username: username
            }, CONFIG.JWT.secret,{
                expiresIn: '1h',
                issuer: 'admin'
            });
            ctx.body = {
                token: token,
                iss: 'admin',
                username: username
            }
        }else{
            var token = jwt.sign({
                username: username
            },CONFIG.JWT.secret,{
                expiresIn: '1h',
            });
            ctx.body = {token: token, username: username}
        }
    }else{
        ctx.throw(401, "Wrong username or password");
    }
});

export default router;