import Router from 'koa-router';
import {verify, authorize, verifyAdmin} from '../middlewares/jwt-verify'
import ImplementedAction from '../models/implemented-action';
import Action from '../models/action';
import Event from '../models/event';
const router = new Router();

router.post('/implemented-action', verify, async (ctx, next)=> {
    let {event} = ctx.request.body;
    let temp = await Event.find({
        where:{
            description: event
        }
    });
    let actions = await ImplementedAction.findAll({
        attributes: ['action'],
        where:{
            related_uid: temp.uid
        }
    });
    ctx.body = actions;
});

router.post('/request-action', verify, authorize, async(ctx, next)=>{
    ctx.body = {message: 'ACCESS GRANTED'}
});

router.get('/all-actions', verify, async(ctx, next)=>{
    ctx.body = await Action.findAll({
        attributes: ['title']
    });
});

router.post('/set-implemented-action', verifyAdmin, async (ctx, next)=>{
    var {action} = ctx.request.body;
    var {func} = ctx.request.body;
    await ImplementedAction.destroy({
        where:{
            related_uid: func
        }
    });
    for(let i = 0; i < action.length; i++){
        ImplementedAction.create({
            action: action[i],
            related_uid: func
        });
    }
    ctx.body = {message: "Success"};
});

router.post('/new-action', verifyAdmin, authorize, async (ctx, next)=>{
    let {actionAffected} = ctx.request.body;
    let {eventAffected} = ctx.request.body;

    let act = await Action.find({
        where:{
            title: actionAffected
        }
    });
    let ev = await Event.find({
        where:{
            description: eventAffected
        }
    });
    let temp = await ImplementedAction.find({
        where:{
            action: actionAffected,
            related_uid: ev.uid
        }
    });

    if(!temp && ev){
        ImplementedAction.create({
            action: actionAffected,
            related_uid: ev.uid
        });
        if(!act){
            Action.create({
                title: actionAffected
            });
        }
        ctx.body = {message: "Success"};
    }else{
        ctx.throw(400, "Action already exists");
    }
});

router.post('/delete-action', verifyAdmin, authorize, async (ctx, next)=>{
    var {actionAffected} = ctx.request.body;
    Action.destroy({
       where:{
           title: actionAffected
       } 
    });
    ImplementedAction.destroy({
        where:{
            action: actionAffected
        }
    });
    ctx.body = {message: "Success"}
});

export default router;