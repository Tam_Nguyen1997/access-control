import Router from 'koa-router';
import {Sequelize} from '../models/db';
import User from '../models/user';
import Privilege from '../models/privilege';
import {verifyAdmin, authorize} from '../middlewares/jwt-verify';

const router = new Router();
const Op = Sequelize.Op;

router.post('/privilege', verifyAdmin, async (ctx, next)=> {
    var {username} = ctx.request.body;
    var user = await User.find({
        attributes: ['uid'],
        where:{
            username: username
        }
    });
    var privilege = await Privilege.findAll({
        attributes: ['action', 'group', 'related_uid'],
        where:{
            who: user.uid
        }
    });
    ctx.body = privilege;
});

router.post('/edit-privilege', verifyAdmin, authorize, async(ctx, next)=>{
    var {usernameAffected} = ctx.request.body;
    var {actionAffected} = ctx.request.body;
    var {group} = ctx.request.body;
    var {eventAffected} = ctx.request.body;

    var user = await User.find({
        where:{
            username: usernameAffected
        }
    });

    Privilege.destroy({
        where:{
            who: user.uid,
            related_uid: eventAffected
        }
    });
    if(actionAffected.length > 0){
        
        for(let i = 0; i < actionAffected.length; i++){
            Privilege.create({
                who: user.uid,
                action: actionAffected[i],
                related_uid: eventAffected
            });
        }
    }
    User.update({
        group_memberships: group
    },{
        where:{
            username: usernameAffected,
        }
    });
    ctx.body = {message: "Success"}
});

router.post('/delete-privilege', verifyAdmin, authorize, async(ctx, next)=>{
    var {usernameAffected} = ctx.request.body;
    var user = await User.find({
        where: {
            username: usernameAffected
        }
    });

    Privilege.destroy({
        where:{
            who: user.uid
        }
    });
    ctx.body = {message: "Success"};
});

router.post('/new-privilege', verifyAdmin, async(ctx, next)=>{
    var {username} = ctx.request.body;
    var {action} = ctx.request.body;
    var action1 = action.action1;
    var action2 = action.action2;
    var user = await User.find({
        where:{
            username: username
        }
    });

    for(let i = 0; i < action1.length; i++){
        Privilege.create({
            who: user.uid,
            action: action1[i],
            related_uid: 1
        });
    }
    
    for(let i = 0; i < action2.length; i++){
        Privilege.create({
            who: user.uid,
            action: action2[i],
            related_uid: 2
        });
    }
   ctx.body = {message: "Success"};
});
export default router;