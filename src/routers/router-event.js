import Router from 'koa-router';
import {verifyAdmin, verify, authorize} from '../middlewares/jwt-verify';
import Event from '../models/event';
import ImplementedAction from '../models/implemented-action';

const router = new Router();

router.get('/all-events', async (ctx, next)=>{
    ctx.body = await Event.findAll({
        attributes: ['description', 'uid']
    });
});

router.post('/event-actions', verify, async (ctx, next)=>{
    var {ev} = ctx.request.body;
    var event = await Event.find({
        where:{
            description: ev
        },
    });
    
    ctx.body = await ImplementedAction.findAll({
        where:{
            related_uid: event.uid
        }
    });
});

router.post('/delete-event', verifyAdmin, authorize, async(ctx, next)=>{
    let {eventAffected} = ctx.request.body;
    let temp = await Event.find({
        where:{
            description: eventAffected
        }
    });
    Event.destroy({
        where:{
            description: eventAffected
        }
    });

    ImplementedAction.destroy({
        where: {
            related_uid: temp.uid
        }
    });
    ctx.body = {message: "Success"};
});

router.post('/new-event', verifyAdmin, authorize, async(ctx, next)=>{
    var {description} = ctx.request.body;
    Event.create({
        description: description
    });
    ctx.body = {message: "Success"};
});
export default router;