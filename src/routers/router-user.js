import Router from 'koa-router';
import {verifyAdmin, authorize} from '../middlewares/jwt-verify';
import User from '../models/user';
import btoa from 'btoa';

const router = new Router();

router.get('/user', verifyAdmin,  async (ctx, next)=> {
    var user = await User.findAll({
        attributes: ['username']
    });
    ctx.body = user;
});

router.post('/group', verifyAdmin, async(ctx, next)=>{
    var {username} = ctx.request.body;
    var groups = await User.find({
        attributes:['group_memberships'],
        where:{
            username: username
        }
    });
    ctx.body = groups
});

router.post('/delete-user', verifyAdmin, authorize, async(ctx, next)=>{
    var {usernameAffected} = ctx.request.body;
    User.destroy({
        where:{
            username: usernameAffected
        }
    });
    ctx.body = {message: "Success"};
});

router.post('/new-user', verifyAdmin, authorize, async(ctx, next)=>{
    var {usernameAffected} = ctx.request.body;
    var {password} = ctx.request.body;
    var {group} = ctx.request.body;
    password = btoa(password);
    var user = await User.find({
        where:{
            username: usernameAffected
        }
    });
    if(!user){
        User.create({
            username: usernameAffected,
            password: password,
            group_memberships: group
        });
        ctx.body = {message: "Success"};
    }else{
        ctx.throw(400, 'Username already exists');
    }
});
export default router;