import compose from 'koa-compose';
import routerLogin from './router-login';
import routerPrivilege from './router-privilege';
import routerUser from './router-user';
import routerImplementedAction from './router-action';
import routerEvent from './router-event';

//We need to convert list of separated route in to the one
let routers = [
  routerLogin,
  routerPrivilege,
  routerUser,
  routerImplementedAction,
  routerEvent,
];

//So we extract the middelware from router
let middleware = [];
routers.forEach((router) => {
  middleware.push(router.routes())
  middleware.push(router.allowedMethods())
});

//Then put them into one router. Magic here! 
export default compose(middleware);
