import jwt from 'jsonwebtoken';
import CONFIG from '../config';
import User from '../models/user';
import Privilge from '../models/privilege';
import Event from '../models/event';

async function verify(ctx, next){
    var {authorization} = ctx.request.header;
    if(!authorization){
      ctx.throw(401, 'Autentication Error');
    }else{
      var parts = authorization.split(' ');
      if(parts[0] != 'Bearer'){
        ctx.throw(401, 'Authentication Error');
      }
      try{
        var obj = jwt.verify(parts[1], CONFIG.JWT.secret);
      }catch(e){
        ctx.throw(401, 'Authentication Error');
      }
    }
    await next();
}

async function verifyAdmin(ctx, next){
  var {authorization} = ctx.request.header;
  if(!authorization){
      ctx.throw(401, 'Autentication Error');
    }else{
      var parts = authorization.split(' ');
      if(parts[0] != 'Bearer'){
        ctx.throw(401, 'Authentication Error');
      }
      try{
        var obj = jwt.verify(parts[1], CONFIG.JWT.secret);
        if(!obj.iss || obj.iss != 'admin'){
          ctx.throw(401, 'Authentication Error');
        }
      }catch(e){
        ctx.throw(401, 'Authentication Error');
      }
    }
    await next();
}

async function authorize(ctx, next){
    var {username} = ctx.request.body;
    var {action} = ctx.request.body;
    var {event} = ctx.request.body;

    let temp = await Event.find({
      where:{
        description: event
      }
    });
    var user = await User.find({
      where:{
        username: username
      }
    });
    if(temp && user){
      var privilege = await Privilge.find({
        where:{
          who: user.uid,
          action: action,
          related_uid: temp.uid,
          status: 4
        }
      });
    }

    if(!privilege || (user.group_memberships & privilege.group <= 0)){
      ctx.throw(401, 'You cannot perform this action');
    }

    await next();
}
export {verify, authorize, verifyAdmin};