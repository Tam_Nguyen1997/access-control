const CONFIG = {
  APP: {
    port: 3000,
    host: 'localhost'
  },
  JWT: {
    secret: 'RUj2J]FcebI`:^SOzv&#x.J<gtZx(-\k'
  },
  DB:{
    database: "privilege",
    username: "root",
    password: "",
    host: "localhost",
    dialect: "mysql",
    timezone: "+07:00"
  },
  WHITELIST:  ['*'],
};

export default CONFIG;