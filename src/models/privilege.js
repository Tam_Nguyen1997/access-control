import {Sequelize, sequelize} from './db';

const Privilege = sequelize.define('Privilege', {
    action: {type: Sequelize.STRING, primaryKey: true},
    group: {type: Sequelize.INTEGER, primaryKey: true, defaultValue: 3},
    related_uid: {type: Sequelize.INTEGER, primaryKey: true},
    status: {type: Sequelize.INTEGER, primaryKey: true, defaultValue: 4},
    who: {type: Sequelize.INTEGER, primaryKey: true}
},
{
    timestamps: false,
    tableName: 'privilege'
});

export default Privilege;