import {Sequelize, sequelize} from './db';

const User = sequelize.define('User', {
    uid: {type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true},
    username: {type: Sequelize.STRING, allowNull: false},
    password: {type: Sequelize.STRING, allowNull: false},
    status: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 4},
    group_memberships: {type: Sequelize.INTEGER, allowNull: false},
    unixperms: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 500},
},
{
    timestamps: false,
    tableName: 'user'
});

export default User;