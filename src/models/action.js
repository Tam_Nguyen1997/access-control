import {Sequelize, sequelize} from './db';

const Action = sequelize.define('User', {
    title: {type: Sequelize.STRING, primaryKey: true},
    type: {type: Sequelize.STRING, allowNull: false, defaultValue: 'object'}
},
{
    timestamps: false,
    tableName: 'action'
});

export default Action;