import {Sequelize, sequelize} from './db';

const ImplementedAction = sequelize.define('ImplementedAction', {
    status: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 4},
    related_uid: {type: Sequelize.INTEGER, primaryKey: true},
    action: {type: Sequelize.STRING, primaryKey: true},
},
{
    timestamps: false,
    tableName: 'Implemented_action'
});

export default ImplementedAction;