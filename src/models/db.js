import Sequelize from "sequelize";
import CONFIG from '../config';
const sequelize = new Sequelize(CONFIG.DB.database, CONFIG.DB.username, CONFIG.DB.password,{
    host: CONFIG.DB.host,
    dialect: CONFIG.DB.dialect,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      },
    logging: false,
    timezone: CONFIG.DB.timezone,
});

export {Sequelize, sequelize}