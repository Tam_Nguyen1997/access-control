import {Sequelize, sequelize} from './db';

const Event = sequelize.define('Event', {
    uid: {type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true},
    status: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 4},
    owner: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 1},
    group: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 3},
    unixperms: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 500},
    description: {type: Sequelize.STRING, allowNull: false},
},
{
    timestamps: false,
    tableName: 'event'
});

export default Event;